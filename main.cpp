#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cstdlib>

using namespace std;
using namespace cv;

int main() {
	VideoCapture cap(CV_CAP_ANY);

	if(!cap.isOpened()) {
		cerr << "Cam0 is not opened." << endl;
		return EXIT_FAILURE;
	}

	namedWindow("Cam0", CV_WINDOW_AUTOSIZE);

	for (;;) {
		Mat frame;
		cap >> frame;
		imshow("Cam0", frame);

		char c = waitKey(10);
		if (c == 'q') {
			break;
		}
	}

	return EXIT_SUCCESS;
}

